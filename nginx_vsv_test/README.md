Role Name
=========

Test ansible role to install and configure nginx

Requirements
------------


Role Variables
--------------

content - path to folder where to store the content. Default: /var/www
domain - domain name for virtualhost. Default: example.com

Dependencies
------------

A list of other roles hosted on Galaxy should go here, plus any details in regards to parameters that may need to be set for other roles, or variables that are used from other roles.

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
         - { role: nginx_vsv_test }

Tests
----------------
py.test -v ./tests/locale_tests.py  
py.test -v --connection=ansible --hosts=servers_group --ansible-inventory=inventory.ini nginx_vsv_test/tests/remote_test.py

License
-------

BSD

Author Information
------------------

An optional section for the role authors to include contact information, or a website (HTML is not allowed).
