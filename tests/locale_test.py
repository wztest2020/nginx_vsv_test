domain = "example.com"

def test_port_80_check_connection(host):
    eip = host.addr(domain)
    assert eip.port(80).is_reachable

def test_port_443_check_connection(host):
    eip = host.addr(domain)
    assert eip.port(443).is_reachable
