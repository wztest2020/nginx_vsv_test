#import os
#import pytest

#import testinfra.utils.ansible_runner

#testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
#    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')

def test_nginx_is_installed(host):
    nginx = host.package("nginx")
    assert nginx.is_installed

def test_nginx_running_and_enabled(host):
    nginx = host.service("nginx")
    assert nginx.is_running
    assert nginx.is_enabled


def test_port_80_is_listening(host):
    socket = host.socket("tcp://80")
    assert(socket.is_listening)

def test_port_443_is_listening(host):
    socket = host.socket("tcp://443")
    assert(socket.is_listening)


def test_port_check_connection(host):
    eip = host.addr("127.0.0.1")
    assert eip.port(80).is_reachable
    assert eip.port(443).is_reachable
